getAllKategori();
function getAllKategori(){
    $.ajax({
        url: host + '/api/kategori',
        type:'GET',
        contentType:'application/json',
        success:function(data){
            $("#kategoriData").html(``);
            for(i = 0; i<data.length; i++){
                $("#kategoriData").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].nama}</td>
                        <td>${data[i].keterangan}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data[i].id})"><i class="bi bi-pencil-square"></i>Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data[i].id})"><i class="bi bi-trash3"></i>Delete</button>
                        </th>
                    </tr>
                `)
            }
        }
    })
}

function form_(id){
    var str = ``;
    if(id){
        $.ajax({
            url: host + '/api/kategori/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str = `Nama : <input class="form-control" type="text" id="nama" value="${data.nama}">`;
                str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
                str += `Keterangan : <input class="form-control" type="text" id="keterangan" value="${data.keterangan}">`;
                str += `<small class="errorField text-danger" type="text" id="errKeterangan"></small> <br>`;
                str += `<hr>
                    <button class="btn btn-warning" onclick="editKategori(${data.id})">edit</button>
                    <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str = `Nama : <input class="form-control" type="text" id="nama">`;
        str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
        str += `Keterangan : <input class="form-control" type="text" id="keterangan">`;
        str += `<small class="errorField text-danger" type="text" id="errKeterangan"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanKategori()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Kategori Form")
}

function batal(){
    $("#mymodal").modal("hide");
}

function simpanKategori(){
    var nama = $("#nama").val();
    var keterangan = $("#keterangan").val();
    var verifNama = false;
    var verifKeterangan = false;
    $("#errNama").text("");
    $("#errKeterangan").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(keterangan == ""){
       $("#errKeterangan").text("*tidak boleh kosong");
       verifKeterangan = true;
    }
    if(verifNama || verifKeterangan){
        return;
    }

    const kategori = {
        nama: nama,
        keterangan: keterangan
    }

    console.log(kategori);

    $.ajax({
        url: host + '/api/kategori',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(kategori),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllKategori();
        }
    })
}

function editKategori(id){
    var nama = $("#nama").val();
    var keterangan = $("#keterangan").val();
    var verifNama = false;
    var verifKeterangan = false;
    $("#errNama").text("");
    $("#errKeterangan").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(keterangan == ""){
       $("#errKeterangan").text("*tidak boleh kosong");
       verifKeterangan = true;
    }
    if(verifNama || verifKeterangan){
        return;
    }

    const kategori = {
        nama: nama,
        keterangan: keterangan
    }

    console.log(kategori);

    $.ajax({
        url: host + '/api/kategori/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(kategori),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllKategori();
        }
    })
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete kategori")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/kategori/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllKategori();
        }
    })
}
