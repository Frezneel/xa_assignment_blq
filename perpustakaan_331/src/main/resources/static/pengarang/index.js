getAllPengarang();
function getAllPengarang(){
    $.ajax({
        url: host + '/api/pengarang',
        type:'GET',
        contentType:'application/json',
        success:function(data){
            $("#pengarangData").html(``);
            for(i = 0; i<data.length; i++){
                $("#pengarangData").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].nama}</td>
                        <td>${data[i].alamat}</td>
                        <td>${data[i].telp}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data[i].id})">Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data[i].id})">Delete</button>
                        </th>
                    </tr>
                `)
            }
        }
    })
}


function form_(id){
    var str = ``;
    if(id){
        $.ajax({
            url: host + '/api/pengarang/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str = `Nama : <input class="form-control" type="text" id="nama" value="${data.nama}">`;
                str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
                str += `Alamat : <input class="form-control" type="text" id="alamat" value="${data.alamat}">`;
                str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
                str += `Telephone : <input class="form-control" type="text" id="telp" value="${data.telp}">`;
                str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editPengarang(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str = `Nama : <input class="form-control" type="text" id="nama">`;
        str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
        str += `Alamat : <input class="form-control" type="text" id="alamat">`;
        str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
        str += `Telephone : <input class="form-control" type="text" id="telp">`;
        str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanPengarang()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Pengarang Form")
}

function simpanPengarang(){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const pengarang = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    console.log(pengarang);

    $.ajax({
        url: host + '/api/pengarang',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(pengarang),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllPengarang();
        }
    })

}

function editPengarang(id){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const pengarang = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    console.log(pengarang);

    $.ajax({
        url: host + '/api/pengarang/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(pengarang),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllPengarang();
        }
    })

}

function batal(){
    $("#mymodal").modal("hide");
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete pengarang")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/pengarang/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllPengarang();
        }
    })
}
