getAllPenerbit();
function getAllPenerbit(){
    $.ajax({
        url: host + '/api/penerbit',
        type:'GET',
        contentType:'application/json',
        success:function(data){
            $("#penerbitData").html(``);
            for(i = 0; i<data.length; i++){
                $("#penerbitData").append(`
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].nama}</td>
                        <td>${data[i].alamat}</td>
                        <td>${data[i].telp}</td>
                        <th>
                            <button class="btn bg-warning" onclick="form_(${data[i].id})"><i class="bi bi-pencil-square"></i>Edit</button>
                            <button class="btn bg-danger" onclick="delete_(${data[i].id})"><i class="bi bi-trash3"></i>Delete</button>
                        </th>
                    </tr>
                `)
            }
        }
    })
}

function form_(id){
    var str = ``;
    if(id){
        $.ajax({
            url: host + '/api/penerbit/' + id,
            type: "get",
            contentType:'application/json',
            async:false,
            success:function(data){
                str = `Nama : <input class="form-control" type="text" id="nama" value="${data.nama}">`;
                str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
                str += `Alamat : <input class="form-control" type="text" id="alamat" value="${data.alamat}">`;
                str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
                str += `Telephone : <input class="form-control" type="text" id="telp" value="${data.telp}">`;
                str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
                str += `<hr>
                        <button class="btn btn-warning" onclick="editPenerbit(${data.id})">edit</button>
                        <button class="btn btn-primary" onclick="batal()">batal</button>`;
            }
        })
    }else{
        str = `Nama : <input class="form-control" type="text" id="nama">`;
        str += `<small class="errorField text-danger" type="text" id="errNama"></small> <br>`;
        str += `Alamat : <input class="form-control" type="text" id="alamat">`;
        str += `<small class="errorField text-danger" type="text" id="errAlamat"></small> <br>`;
        str += `Telephone : <input class="form-control" type="text" id="telp">`;
        str += `<small class="errorField text-danger" type="text" id="errTelp"></small> <br>`;
        str += `<hr>
                <button class="btn btn-success" onclick="simpanPenerbit()">simpan</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    }

    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Penerbit Form")
}

function simpanPenerbit(){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const penerbit = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    console.log(penerbit);

    $.ajax({
        url: host + '/api/penerbit',
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(penerbit),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllPenerbit();
        }
    })
}
function editPenerbit(id){
    var nama = $("#nama").val();
    var alamat = $("#alamat").val();
    var telp = $("#telp").val();
    var verifNama = false;
    var verifAlamat = false;
    var verifTelp = false;
    $("#errNama").text("");
    $("#errAlamat").text("");
    $("#errTelp").text("");

    if(nama == ""){
       $("#errNama").text("*tidak boleh kosong");
       verifNama = true;
    }
    if(alamat == ""){
       $("#errAlamat").text("*tidak boleh kosong");
       verifAlamat = true;
    }
    if(telp == ""){
       $("#errTelp").text("*tidak boleh kosong");
       verifTelp = true;
    }
    if(verifNama || verifAlamat || verifTelp){
        return
    }

    const penerbit = {
        nama: nama,
        alamat: alamat,
        telp: telp,
    }

    console.log(penerbit);

    $.ajax({
        url: host + '/api/penerbit/' + id,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(penerbit),
        contentType: "application/json",
        success: function(result){
            console.log(result);
            $('#mymodal').modal('hide');
            getAllPenerbit();
        }
    })
}

function batal(){
    $("#mymodal").modal("hide");
}

function delete_(id){
    var str = ``;
    str = `<h5>Apakah anda yakin ingin menghapus data ini?</h5>`;
    str += `<hr><button class="btn btn-danger" onclick="deleteData(${id})">hapus</button>
                <button class="btn btn-primary" onclick="batal()">batal</button>`;
    $(".modal-body").html(str);
    $("#mymodal").modal("show");
    $(".modal-title").html("Delete penerbit")
}

function deleteData(id){
    $.ajax({
        url: host + '/api/penerbit/' + id,
        type:'DELETE',
        contentType:'application/json',
        success:function(result){
            alert("Berhasil");
            $("#mymodal").modal("hide");
            getAllPenerbit();
        }
    })
}
