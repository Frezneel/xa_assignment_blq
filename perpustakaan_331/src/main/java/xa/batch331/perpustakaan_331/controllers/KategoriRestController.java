package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.services.KategoriService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class KategoriRestController {

    @Autowired
    private KategoriService kategoriService;

    @GetMapping("/kategori")
    public ResponseEntity<List<Kategori>> getAllKategori(){
        try {
            List<Kategori> kategoriList = this.kategoriService.getAllKategori();
            return new ResponseEntity<>(kategoriList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/kategori/{id}")
    public ResponseEntity<Optional<Kategori>> getKategoriById(@PathVariable("id")Long id){
        try {
            Optional<Kategori> kategori = this.kategoriService.getKategoriById(id);
            if (kategori.isPresent()){
                return new ResponseEntity<>(kategori, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/kategori/{id}")
    public ResponseEntity<?> updateKategori(@RequestBody Kategori kategori, @PathVariable("id")Long id){
        try {
            Optional<Kategori> kategori_data = this.kategoriService.getKategoriById(id);
            if (kategori_data.isPresent()){
                kategori.setId(id);
                kategori.setCreated_by(kategori_data.get().getCreated_by());
                kategori.setCreated_on(kategori_data.get().getCreated_on());
                kategori.setModified_by(1L);
                kategori.setModified_on(new Date());
                this.kategoriService.saveKategori(kategori);
                return new ResponseEntity<>(kategori, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/kategori/{id}")
    public ResponseEntity<?> deleteKategori (@PathVariable Long id){
        try {
            Optional<Kategori> kategori = this.kategoriService.getKategoriById(id);
            if (kategori.isPresent()){
                kategori.get().setDeleted_by(1L);
                kategori.get().setDeleted_on(new Date());
                kategori.get().setIs_delete(true);
                this.kategoriService.saveKategori(kategori.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/kategori")
    public ResponseEntity<Kategori> addKategori(@RequestBody Kategori kategori){
        try {
            kategori.setCreated_on(new Date());
            kategori.setCreated_by(1L);
            this.kategoriService.saveKategori(kategori);
            return new ResponseEntity<>(kategori, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
