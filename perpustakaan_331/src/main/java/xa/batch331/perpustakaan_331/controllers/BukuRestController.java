package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Buku;
import xa.batch331.perpustakaan_331.services.BukuService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class BukuRestController {

    @Autowired
    private BukuService bukuService;

    @GetMapping("/buku")
    public ResponseEntity<List<Buku>> getAllBuku(){
        try {
            List<Buku> bukuList = this.bukuService.getAllBuku();
            return new ResponseEntity<>(bukuList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/buku/{id}")
    public ResponseEntity<Optional<Buku>> getBukuById(@PathVariable("id")Long id){
        try {
            Optional<Buku> buku = this.bukuService.getBukuById(id);
            if (buku.isPresent()){
                return new ResponseEntity<>(buku, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/buku/{id}")
    public ResponseEntity<?> updateBuku(@RequestBody Buku buku, @PathVariable("id")Long id){
        try {
            Optional<Buku> buku_data = this.bukuService.getBukuById(id);
            if (buku_data.isPresent()){
                buku.setId(id);
                buku.setCreated_by(buku_data.get().getCreated_by());
                buku.setCreated_on(buku_data.get().getCreated_on());
                buku.setModified_by(1L);
                buku.setModified_on(new Date());
                this.bukuService.saveBuku(buku);
                return new ResponseEntity<>(buku, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/buku/{id}")
    public ResponseEntity<?> deleteBuku (@PathVariable Long id){
        try {
            Optional<Buku> buku = this.bukuService.getBukuById(id);
            if (buku.isPresent()){
                buku.get().setDeleted_by(1L);
                buku.get().setDeleted_on(new Date());
                buku.get().setIs_delete(true);
                this.bukuService.saveBuku(buku.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/buku")
    public ResponseEntity<Buku> addBuku(@RequestBody Buku buku){
        try {
            buku.setCreated_on(new Date());
            buku.setCreated_by(1L);
            this.bukuService.saveBuku(buku);
            return new ResponseEntity<>(buku, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
