package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Buku;
import xa.batch331.perpustakaan_331.models.KategoriBuku;
import xa.batch331.perpustakaan_331.services.KategoriBukuService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v2")
public class KategoriBukuRestController {

    @Autowired
    private KategoriBukuService kategoriBukuService;

    @GetMapping("/kategori_buku")
    public ResponseEntity<List<KategoriBuku>> getAllKategoriBuku(){
        try {
            List<KategoriBuku> kategoriBukuList = this.kategoriBukuService.getAllKategoriBuku();
            return new ResponseEntity<>(kategoriBukuList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/kategori_buku/{id}")
    public ResponseEntity<Optional<KategoriBuku>> getKategoriBukuById(@PathVariable("id")Long id){
        try {
            Optional<KategoriBuku> kategoriBuku = this.kategoriBukuService.getKategoriBukuById(id);
            if (kategoriBuku.isPresent()){
                return new ResponseEntity<>(kategoriBuku, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/kategori_buku/{id}")
    public ResponseEntity<?> updateKategoriBuku(@RequestBody KategoriBuku kategoriBuku, @PathVariable("id")Long id){
        try {
            Optional<KategoriBuku> kategoriBuku_data = this.kategoriBukuService.getKategoriBukuById(id);
            if (kategoriBuku_data.isPresent()){
                kategoriBuku.setId(id);
                kategoriBuku.setCreated_by(kategoriBuku_data.get().getCreated_by());
                kategoriBuku.setCreated_on(kategoriBuku_data.get().getCreated_on());
                kategoriBuku.setModified_by(1L);
                kategoriBuku.setModified_on(new Date());
                this.kategoriBukuService.saveKategoriBuku(kategoriBuku);
                return new ResponseEntity<>(kategoriBuku, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/kategori_buku/{id}")
    public ResponseEntity<?> deleteKategoriBuku (@PathVariable Long id){
        try {
            Optional<KategoriBuku> kategoriBuku = this.kategoriBukuService.getKategoriBukuById(id);
            if (kategoriBuku.isPresent()){
                kategoriBuku.get().setDeleted_by(1L);
                kategoriBuku.get().setDeleted_on(new Date());
                kategoriBuku.get().setIs_delete(true);
                this.kategoriBukuService.saveKategoriBuku(kategoriBuku.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/kategori_buku")
    public ResponseEntity<KategoriBuku> addKategoriBuku(@RequestBody KategoriBuku kategoriBuku){
        try {
            kategoriBuku.setCreated_on(new Date());
            kategoriBuku.setCreated_by(1L);
            this.kategoriBukuService.saveKategoriBuku(kategoriBuku);
            return new ResponseEntity<>(kategoriBuku, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
