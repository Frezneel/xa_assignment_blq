package xa.batch331.perpustakaan_331.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("kategori")
public class KategoriController {

    @GetMapping
    public ModelAndView index(){
        ModelAndView view = new ModelAndView("kategori/index");
        return view;
    }
}
