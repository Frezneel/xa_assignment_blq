package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.models.Pengarang;
import xa.batch331.perpustakaan_331.services.PengarangService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class PengarangRestController {

    @Autowired
    private PengarangService pengarangService;

    @GetMapping("/pengarang")
    public ResponseEntity<List<Pengarang>> getAllPengarang(){
        try {
            List<Pengarang> pengarangList = this.pengarangService.getAllPengarang();
            return new ResponseEntity<>(pengarangList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pengarang/{id}")
    public ResponseEntity<Optional<Pengarang>> getPengarangById(@PathVariable("id")Long id){
        try {
            Optional<Pengarang> pengarang = this.pengarangService.getPengarangById(id);
            if (pengarang.isPresent()){
                return new ResponseEntity<>(pengarang, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/pengarang/{id}")
    public ResponseEntity<?> updatePengarang(@RequestBody Pengarang pengarang, @PathVariable("id")Long id){
        try {
            Optional<Pengarang> pengarang_data = this.pengarangService.getPengarangById(id);
            if (pengarang_data.isPresent()){
                pengarang.setId(id);
                pengarang.setCreated_by(pengarang_data.get().getCreated_by());
                pengarang.setCreated_on(pengarang_data.get().getCreated_on());
                pengarang.setModified_by(1L);
                pengarang.setModified_on(new Date());
                this.pengarangService.savePengarang(pengarang);
                return new ResponseEntity<>(pengarang, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/pengarang/{id}")
    public ResponseEntity<?> deletePengarang (@PathVariable Long id){
        try {
            Optional<Pengarang> pengarang = this.pengarangService.getPengarangById(id);
            if (pengarang.isPresent()){
                pengarang.get().setDeleted_by(1L);
                pengarang.get().setDeleted_on(new Date());
                pengarang.get().setIs_delete(true);
                this.pengarangService.savePengarang(pengarang.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/pengarang")
    public ResponseEntity<Pengarang> addPengarang(@RequestBody Pengarang pengarang){
        try {
            pengarang.setCreated_on(new Date());
            pengarang.setCreated_by(1L);
            this.pengarangService.savePengarang(pengarang);
            return new ResponseEntity<>(pengarang, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
