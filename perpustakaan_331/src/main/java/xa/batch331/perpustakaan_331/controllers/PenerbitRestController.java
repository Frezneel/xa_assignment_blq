package xa.batch331.perpustakaan_331.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan_331.models.Penerbit;
import xa.batch331.perpustakaan_331.services.PenerbitService;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class PenerbitRestController {

    @Autowired
    private PenerbitService penerbitService;

    @GetMapping("/penerbit")
    public ResponseEntity<List<Penerbit>> getAllPenerbit(){
        try {
            List<Penerbit> penerbitList = this.penerbitService.getAllPenerbit();
            return new ResponseEntity<>(penerbitList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/penerbit/{id}")
    public ResponseEntity<Optional<Penerbit>> getPenerbitById(@PathVariable("id")Long id){
        try {
            Optional<Penerbit> penerbit = this.penerbitService.getPenerbitById(id);
            if (penerbit.isPresent()){
                return new ResponseEntity<>(penerbit, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/penerbit/{id}")
    public ResponseEntity<?> updatePenerbit(@RequestBody Penerbit penerbit, @PathVariable("id")Long id){
        try {
            Optional<Penerbit> penerbit_data = this.penerbitService.getPenerbitById(id);
            if (penerbit_data.isPresent()){
                penerbit.setId(id);
                penerbit.setCreated_by(penerbit_data.get().getCreated_by());
                penerbit.setCreated_on(penerbit_data.get().getCreated_on());
                penerbit.setModified_by(1L);
                penerbit.setModified_on(new Date());
                this.penerbitService.savePenerbit(penerbit);
                return new ResponseEntity<>(penerbit, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/penerbit/{id}")
    public ResponseEntity<?> deletePenerbit (@PathVariable Long id){
        try {
            Optional<Penerbit> penerbit = this.penerbitService.getPenerbitById(id);
            if (penerbit.isPresent()){
                penerbit.get().setDeleted_by(1L);
                penerbit.get().setDeleted_on(new Date());
                penerbit.get().setIs_delete(true);
                this.penerbitService.savePenerbit(penerbit.get());
                Map<String, Object> result = new HashMap<>();
                result.put("status", "sukses");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/penerbit")
    public ResponseEntity<Penerbit> addPenerbit(@RequestBody Penerbit penerbit){
        try {
            penerbit.setCreated_on(new Date());
            penerbit.setCreated_by(1L);
            this.penerbitService.savePenerbit(penerbit);
            return new ResponseEntity<>(penerbit, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
