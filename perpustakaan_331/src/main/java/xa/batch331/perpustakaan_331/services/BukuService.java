package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Buku;
import xa.batch331.perpustakaan_331.repositories.BukuRepo;

import java.util.List;
import java.util.Optional;

@Service
public class BukuService {

    @Autowired
    private BukuRepo bukuRepo;

    public List<Buku> getAllBuku(){
        return this.bukuRepo.getAllBuku();
    }

    public Optional<Buku> getBukuById(Long id){
        return this.bukuRepo.getBukuById(id);
    }

    public void saveBuku(Buku buku){
        this.bukuRepo.save(buku);
    }

    public void deleteBuku(Long id){
        this.bukuRepo.deleteById(id);
    }

}
