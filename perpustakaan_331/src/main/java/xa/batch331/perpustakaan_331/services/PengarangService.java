package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Pengarang;
import xa.batch331.perpustakaan_331.repositories.PengarangRepo;

import java.util.List;
import java.util.Optional;

@Service
public class PengarangService {
    @Autowired
    private PengarangRepo pengarangRepo;

    public List<Pengarang> getAllPengarang(){
        return this.pengarangRepo.getAllPengarang();
    }

    public Optional<Pengarang> getPengarangById(Long id){
        return this.pengarangRepo.getPengarangById(id);
    }

    public void savePengarang(Pengarang pengarang){
        this.pengarangRepo.save(pengarang);
    }

    public void deletePengarang(Long id){
        this.pengarangRepo.deleteById(id);
    }
}
