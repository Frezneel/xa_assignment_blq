package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Kategori;
import xa.batch331.perpustakaan_331.repositories.KategoriRepo;

import java.util.List;
import java.util.Optional;

@Service
public class KategoriService {

    @Autowired
    private KategoriRepo kategoriRepo;

    public List<Kategori> getAllKategori(){
        return this.kategoriRepo.getAllKategori();
    }

    public Optional<Kategori> getKategoriById(Long id){
        return this.kategoriRepo.getKategoriById(id);
    }

    public void saveKategori(Kategori kategori){
        this.kategoriRepo.save(kategori);
    }

    public void deleteKategori(Long id){
        this.kategoriRepo.deleteById(id);
    }

}
