package xa.batch331.perpustakaan_331.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan_331.models.Penerbit;
import xa.batch331.perpustakaan_331.repositories.PenerbitRepo;

import java.util.List;
import java.util.Optional;

@Service
public class PenerbitService {
    @Autowired
    private PenerbitRepo penerbitRepo;

    public List<Penerbit> getAllPenerbit(){
        return this.penerbitRepo.getAllPenerbit();
    }

    public Optional<Penerbit> getPenerbitById(Long id){
        return this.penerbitRepo.getPenerbitById(id);
    }

    public void savePenerbit(Penerbit penerbit){
        this.penerbitRepo.save(penerbit);
    }

    public void deletePenerbit(Long id){
        this.penerbitRepo.deleteById(id);
    }
}
