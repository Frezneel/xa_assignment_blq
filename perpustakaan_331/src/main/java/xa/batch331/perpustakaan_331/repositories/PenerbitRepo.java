package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Penerbit;

import java.util.List;
import java.util.Optional;

@Repository
public interface PenerbitRepo extends JpaRepository<Penerbit, Long> {
    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false", nativeQuery = true)
    List<Penerbit> getAllPenerbit();

    @Query(value = "SELECT * FROM penerbit WHERE penerbit.is_delete = false AND penerbit.id = :id", nativeQuery = true)
    Optional<Penerbit> getPenerbitById(Long id);
}
