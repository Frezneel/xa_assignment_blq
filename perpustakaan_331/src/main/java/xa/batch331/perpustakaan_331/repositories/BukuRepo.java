package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Buku;

import java.util.List;
import java.util.Optional;

@Repository
public interface BukuRepo extends JpaRepository<Buku, Long> {

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false", nativeQuery = true)
    List<Buku> getAllBuku();

    @Query(value = "SELECT * FROM buku WHERE buku.is_delete = false AND buku.id = :id", nativeQuery = true)
    Optional<Buku> getBukuById(Long id);
}
