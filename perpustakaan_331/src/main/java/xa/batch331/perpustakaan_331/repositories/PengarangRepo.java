package xa.batch331.perpustakaan_331.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan_331.models.Pengarang;

import java.util.List;
import java.util.Optional;

@Repository
public interface PengarangRepo extends JpaRepository<Pengarang, Long> {
    @Query(value = "SELECT * FROM pengarang WHERE pengarang.is_delete = false", nativeQuery = true)
    List<Pengarang> getAllPengarang();

    @Query(value = "SELECT * FROM pengarang WHERE pengarang.is_delete = false AND pengarang.id = :id", nativeQuery = true)
    Optional<Pengarang> getPengarangById(Long id);
}
