//19. Tentukan apakah kalimat ini adalah Pangram* atau bukan
//“Sphinx of black quartz, judge my vow” => pangrams
//“Brawny gods just flocked up to quiz and vex him”
//“Check back tomorrow; I will see if the book has arrived.”
//*Pangram adalah kata atau kalimat yang mengandung setiap abjad alphabet,
//contohnya “A quick brown fox jumps over the lazy dog”

//SELESAI

import java.util.Scanner;

public class Soal19_Pangrams {
    final private static String huruf = "abcdefghijklmnopqrstuvwxyz";
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("======+ Panagrams +========");
        System.out.println("Contoh Input : Saya Rajin Ngoding");
        System.out.println("Masukkan = ");
        String answer = sc.nextLine();

        answer = answer.trim().toLowerCase();

        char[] answerChar = answer.trim().toCharArray();

        boolean output = cekPanagram(answerChar);

        if (output){
            System.out.println("Output = Panagrams");
        }else {
            System.out.println("Output = Bukan Panagrams");
        }
    }

    public static boolean cekPanagram(char[] input){
        char[] hurufChar = huruf.toCharArray();
        int panjangInput = input.length;
        int panjangHuruf = hurufChar.length;
        int hitungHuruf = 0;

        for (int i = 0; i < panjangHuruf; i++) {
            for (int j = 0; j < panjangInput; j++) {
                if (hurufChar[i] == input[j]){
                    hitungHuruf++;
                    break;
                }
            }
        }
        if (hitungHuruf == huruf.length()){
            return true;
        }else {
            return false;
        }

    }
}
