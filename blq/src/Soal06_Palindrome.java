// 6.	Tanpa menggunakan fungsi Reverse, buatlah fungsi untuk menentukan apakah sebuah kata adalah palindrome* atau tidak
//*Palindrome adalah kata yang jika dibalik tetap sama, contohnya “katak”, “12021”, “malam”

//SELESAI

import java.util.Scanner;

public class Soal06_Palindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        String input = "";
        while (flag_input) {
            System.out.println("==> Palindrome ");
            System.out.println("Masukkan kata atau angka : ");
            input = sc.nextLine();
            if (input.isEmpty()){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }

        String balik = "";
        for (int i = input.length(); i > 0 ; i--) {
            String kata = input.substring(i-1, i);
            balik += kata;
        }

        if (balik.toLowerCase().equals(input.toLowerCase())){
            System.out.println("Palindrome");
        }else {
            System.out.println("Bukan Palindrome");
        }
    }
}
