//18.	Donna sangat menyukai kue, tetapi ia ingin badannya tetap bagus sehingga setiap Donna memakan kue dengan n kalori,
// ia perlu berolahraga selama 0.1 x n x j menit,

//SELESAI

import java.util.*;

public class Soal18_KalkulatorDonna {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int mulai_olahraga = 18;
        Map<Integer, Integer> dataMakan = new HashMap<>();
        dataMakan.put(9, 30);
        dataMakan.put(13, 20);
        dataMakan.put(15, 50);
        dataMakan.put(17, 80);
        System.out.println("Data Makan dan Kalori kue yang dimakan Donna");
        System.out.println("Jam |" + "| Kalori" );

        double waktu_olahraga = 0;
        for (Map.Entry<Integer, Integer> item: dataMakan.entrySet()) {
            System.out.println(item.getKey() + " || " + item.getValue());
            waktu_olahraga += 0.1 * item.getValue() * (mulai_olahraga - item.getKey());
        }
        System.out.println("Donna berolahraga selama : " + waktu_olahraga + " Menit");

        double banyak_air = ((waktu_olahraga / 30) * 100) + 500;

        System.out.println("banyak air yang diminum Donna adalah " + banyak_air + " cc");
    }
}
