//16.	Kamu dan 3 temanmu makan-makan di restoran, dan kalian memesan beberapa menu yang nanti pembayarannya akan dibagi rata.

// contoh input :
// tuna sandwich(ikan) 30000
// pizza tuna(ikan) 60000
// salad buah 40000
// kentang goreng 24000

//SELESAI

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Soal16_PesanMakanan {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean flag_pesanan = true;
        Map<String, String> dataPesanan = new HashMap<>();
        while (flag_pesanan){
            System.out.println("Masukkan pesanan makanan :");
            System.out.println("*jika mengandung ikan tambahkan (ikan)");
            System.out.println("*contoh: tuna sandwich(ikan)");
            String inputMakanan = sc.nextLine();
            System.out.println("Masukkan harga makanan :");
            String inputHarga = sc.nextLine();
            dataPesanan.put(inputMakanan, inputHarga);
            System.out.println("Apakah ada lagi? y/n");
            String inputUlang = sc.nextLine();
            if (inputUlang.toLowerCase().equals("n")){
                flag_pesanan = false;
            }
        }

        //Olah Data
        //Deklarasi aku dan 3 teman
        Map<String, Double> dataPembayaran = new HashMap<>();
        dataPembayaran.put("saya", 0.0);
        dataPembayaran.put("teman 1", 0.0);
        dataPembayaran.put("teman 2", 0.0);
        dataPembayaran.put("teman 3 (alergi ikan)", 0.0);
        double pajak = 0.1; // 10%
        double service = 0.05; // 5%
        for (Map.Entry<String, String> item: dataPesanan.entrySet()) {
            double harga = Double.parseDouble(item.getValue());
            if (item.getKey().contains("ikan")){
                double bagiHarga = (harga - (harga * pajak) + (harga * service)) / 3;
                for (Map.Entry<String, Double> data: dataPembayaran.entrySet()) {
                    double lastValue = data.getValue();
                    if (data.getKey().contains("alergi ikan")){
                        data.setValue(lastValue + 0);
                    }else {
                        data.setValue(lastValue + bagiHarga);
                    }
                }
            }else {
                double bagiHarga = (harga - (harga * pajak) + (harga * service)) / 4;
                for (Map.Entry<String, Double> data: dataPembayaran.entrySet()) {
                    double lastValue = data.getValue();
                    data.setValue(lastValue + bagiHarga);
                }
            }
        }

        for (Map.Entry<String, Double> data: dataPembayaran.entrySet()) {
            System.out.println(data.getKey() + " = " + data.getValue());
        }

    }
}
