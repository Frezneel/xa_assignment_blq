//12. Input: 1 2 1 3 4 7 1 1 5 6 1 8
// Output: 1 1 1 1 1 2 3 4 5 6 7 8
// *selesaikan dengan TIDAK menggunakan fungsi Sort


//SELESAI

import java.util.Scanner;

public class Soal12_Sort {
    public static void main(String[] args) {
        System.out.println("==== Mengurutkan Terkeci ke terbesar ====");
        System.out.println("Masukkan Deret Angka : ");
        System.out.println("Contoh : 1 2 3 4 5");
        System.out.println("Masukkan : ");
        Scanner input = new Scanner(System.in);

        String inputNilai = input.nextLine();

        String[] textArray = inputNilai.split(" ");
        int[] nilai = new int[textArray.length];
        for (int i = 0; i < textArray.length; i++) {
            nilai[i] = Integer.parseInt(textArray[i]);
        }

        int panjang = nilai.length;
        int counter = 0;
        int[] urut = new int[panjang];

        for (int i = 0; i < panjang ; i++) {
            for (int j = 0; j < panjang; j++) {
                if (nilai[i] != nilai[j]){
                    if (nilai[i] > nilai[j]){
                        counter++;
                    }
                }
            }
            urut[i] = counter;
            counter = 0;
        }
        System.out.print("Output = ");
        for (int i = 0; i < panjang; i++) {
            for (int j = 0; j < panjang; j++) {
                if (i == urut[j]){
                    System.out.print(nilai[j] + " ");
                }
            }
        }
    }
}
