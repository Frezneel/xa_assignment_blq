//22. Sederet lilin memiliki perbandingan laju meleleh mengikuti deret Fibonacci.
//Jika diberikan panjang lilin awal masing-masing adalah
//3 3 9 6 7 8 23
//Tentukan lilin mana yang paling pertama habis meleleh.

//SELESAI

import java.util.Scanner;

public class Soal22_LilinMeleleh {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        String input = "";
        while (flag_input) {
            System.out.println("====> Menemukan lilin pertama x mati dengan durasi fibonacci");
            System.out.println("Masukkan nilai tinggi lilin: ");
            input = sc.nextLine();
            if (input.isEmpty()){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }
        //Olah Input
        String[] dataInput = input.split(" ");

        //Olah Fibonacci
        boolean flag_fibo = true;
        int[] data_fibo = new int[dataInput.length];
        int countData = 0;
        int before = 0;

        while (flag_fibo) {
            if (countData == 0){
                data_fibo[0] = 1;
                countData++;
            }else {
                data_fibo[countData] = data_fibo[countData-1] + before;
                before = data_fibo[countData-1];
                countData++;
            }
            if (countData >= dataInput.length) {
                flag_fibo = false;
            }
        }

        //Olah data
        double[] durasi_lilin = new double[dataInput.length];
        int posisi_lilin = 0;
        double lama_lilin = 0;
        String out_durasi = "";
        for (int i = 0; i <dataInput.length ; i++) {
            durasi_lilin[i] = Double.parseDouble(dataInput[i]) / data_fibo[i];
            if (i > 0 && durasi_lilin[i] < lama_lilin){
                posisi_lilin = i;
                lama_lilin = durasi_lilin[i];
            }else if(i == 0){
                posisi_lilin = i;
                lama_lilin = durasi_lilin[i];
            }
            out_durasi += "(" + durasi_lilin[i] + ") ";
        }
        System.out.println("Durasi lilin habis berdasarkan posisi sesuai input : \n" + out_durasi);
        System.out.println("Lilin yang pertama habis berada diposisi ke-" + (posisi_lilin+1) +", panjang " + dataInput[posisi_lilin] + ", habis dalam waktu " + durasi_lilin[posisi_lilin] + " detik");

    }
}
