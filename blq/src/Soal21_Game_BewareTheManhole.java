// Nomor 21 masih meragukan untuk outputnya
// s_____o___f

//SELESAI

import java.util.Scanner;

public class Soal21_Game_BewareTheManhole {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        String inputPola = "";
        while (flag_input){
            System.out.println("Masukkan Pola Lintasan");
            System.out.println("S: Start, F: Finish");
            System.out.println("Contoh : s_____o___f");
            System.out.println("input = ");
            inputPola = sc.nextLine();
            if (inputPola.contains("s") && inputPola.contains("f")){
                flag_input = false;
            }else {
                System.out.println("Invalid input!!");
            }
        }

        char[] polaChar = inputPola.toCharArray(); // Karena dibaca per karakter
        int skip = 0; // untuk loncat
        int energy = 0; // energi ketika lompat
        boolean find = true;
        String output = ""; // tampungan print out

        //olah data
        int i = 1;
        while (find){
            if ((polaChar.length - 1) <= i+skip){
                find = false;
                break;
            }else if(polaChar.length <= 2){
                find = false;
                output = "Pola Lintasan Kosong";
                break;
            }
            char pola = polaChar[(i+skip)];
            char polaNext = polaChar[(i+skip) + 1];
            int last = polaChar.length - (i+skip);
            if (pola == '_' && last == 3 && energy >= 2){
                energy -=2;
                skip +=2;
                output += "J";
                i++;
            }
            else if(pola == '_' && (polaNext == '_' || polaNext == 'f')){
               energy ++;
               output += "W";
               i++;
           }
           else if (pola == '_' && polaNext == 'o' && energy >= 2){
               energy -= 2;
               skip += 2;
               output += "J";
               i++;
           }
           else if (pola == 'o') {
               find = false;
               output = "failed";
               break;
           }
        }
        System.out.println("Output : "+ output);
    }
}
