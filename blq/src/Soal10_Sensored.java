// 10. Sensor bagian tengah
// Contoh :
// input: Susilo Bambang Yudhoyono, Output : S***o B***g Y***o
// Rani Tiara, R***i T***a

//SELESAI

import java.util.Scanner;

public class Soal10_Sensored {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        String input = "";
        while (flag_input) {
            System.out.println("==> Sensored Kata/kalimat ");
            System.out.println("Masukkan kalimat atau kata : ");
            input = sc.nextLine();
            if (input.isEmpty()){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }

        String output = "";
        String[] split = input.split(" ");
        for (int i = 0; i < split.length; i++) {
            String huruf_depan = split[i].substring(0, 1);
            String huruf_belakang = split[i].substring(split[i].length()-1, split[i].length());
            output += huruf_depan + "***" + huruf_belakang + " ";
        }
        System.out.println("Output = " + output);
    }
}
