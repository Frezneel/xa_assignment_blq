//14. Deret: 3 9 0 7 1 2 4
//N = 3	 7 1 2 4 3 9 0
//N = 1	 9 0 7 1 2 4 1

//SELESAI

import java.util.Scanner;

public class Soal14_Rotasi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("=====> Rotasi Deret <=====");
        System.out.println("input nilai deret :");
        String inputDeret = sc.nextLine();
        String[] splitDeret = inputDeret.split(" ");
        int[] bilanganBulat = new int[splitDeret.length];
        for (int i = 0; i < splitDeret.length; i++) {
            bilanganBulat[i] = Integer.parseInt(splitDeret[i]);
        }

        System.out.print("input jumlah rotasi = ");
        int inputRotasi = sc.nextInt();
        int[] rotasi = new int[splitDeret.length];
        //Olah data
        int ambilAwal = 0;
        for (int i = 0; i < inputRotasi; i++) {
            for (int j = 0; j < bilanganBulat.length; j++) {
                ambilAwal = bilanganBulat[0];
                if (j == bilanganBulat.length - 1) {
                    rotasi[j] = ambilAwal;
                } else {
                    rotasi[j] = bilanganBulat[j+1];
                }
            }
            for (int j = 0; j < rotasi.length; j++) {
                bilanganBulat[j] = rotasi[j];
            }
        }

        //Output
        System.out.println("Output rotasi ("+ inputRotasi +") = ");
        for (int i = 0; i < bilanganBulat.length; i++) {
            System.out.print(bilanganBulat[i]);
        }
    }
}
