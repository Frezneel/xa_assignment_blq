//11.	Input: Afrika
// Output:
// ***a***
// ***k***
// ***i***
// ***r***
// ***f***
// ***A***

//SELESAI


import java.util.Scanner;

public class Soal11_reverse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan kata : ");
        String input = sc.nextLine();

        for (int i = input.length(); i > 0; i--) {
            for (int j = 0; j < (input.length()/2); j++) {
                System.out.print("*");
            }
            String kata = input.substring(i-1, i);
            System.out.print(kata);
            for (int j = 0; j < (input.length()/2); j++) {
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
