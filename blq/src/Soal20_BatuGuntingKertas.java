//20. A dan B bermain suit Gunting Batu Kertas.
// player A : G G G
// Player B : K K B

// SELESAI

import java.util.Scanner;

public class Soal20_BatuGuntingKertas {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan jarak : ");
        int jarak = sc.nextInt();
        sc.nextLine();
        String output = "";
        for (int i = 0; i < 3; i++) {
            System.out.println("B: Batu, G: Gunting, K: Kertas");
            System.out.println("Player A : ");
            String playerA = sc.nextLine();
            char A = playerA.charAt(0);
            System.out.println("B: Batu, G: Gunting, K: Kertas");
            System.out.println("Player B : ");
            String playerB = sc.nextLine();
            char B = playerB.charAt(0);
            boolean isA = false;
            boolean isB = false;

            if (A == 'B' && B == 'G'){
                isA = true;
                isB = false;
            } else if (A == 'B' && B == 'K') {
                isA = false;
                isB = true;
            } else if (A == 'G' && B == 'B'){
                isA = false;
                isB = true;
            } else if (A == 'G' && B == 'K') {
                isA = true;
                isB = false;
            } else if (A == 'K' && B == 'B'){
                isA = true;
                isB = false;
            } else if (A == 'K' && B == 'G'){
                isA = false;
                isB = true;
            }else {
                continue;
            }

            if (isA){
                jarak -= 2;
            }else {
                jarak += 1;
            }

            if (isB){
                jarak -= 2;
            }else {
                jarak += 1;
            }

            if (jarak == 0){
                output = isA == true ? "Player A Menang" : "Player B Menang";
                break;
            }else{
                output = "Draw";
            }
        }

        System.out.println(output);

    }
}
