//9.
//N = 3  3 6 9
//N = 4	 4 8 12 16
//N = 5	 5 10 15 20 25

//SELESAI

import java.util.Scanner;

public class Soal09_DeretN {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        int n = 0;
        while (flag_input) {
            System.out.println("Masukkan Nilai (n) untuk membuat deret: ");
            n = sc.nextInt();
            if (n < 1){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }

        for (int i = 1; i <= n; i++) {
            String output = "";
            for (int j = 1; j <= i; j++) {
                int nilai = i * j;
                if (output.length() < 1){
                    output += nilai;
                }else{
                    output += ", " + nilai;
                }
            }
            System.out.println("N = " + i + " => "+ output);
        }

    }
}
