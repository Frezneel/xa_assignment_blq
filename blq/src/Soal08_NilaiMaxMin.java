//8. Tentukan nilai minimal dan maksimal dari penjumlahan 4 komponen deret ini
//Input : 1 2 4 7 8 6 9

//SELESAI

import java.util.Scanner;

public class Soal08_NilaiMaxMin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Masukkan nilai deret : ");
        String input = sc.nextLine();
        String[] split_input = input.split(" ");
        int[] deret = new int[split_input.length];
        for (int i = 0; i < split_input.length; i++) {
            deret[i] = Integer.parseInt(split_input[i]);
        }

        int jumlahKomponen = 4;
        int minimal = Integer.MAX_VALUE;
        int maksimal = Integer.MIN_VALUE;

        // Menentukan nilai minimal dan maksimal
        for (int i = 0; i <= deret.length - jumlahKomponen; i++) {
            int jumlah = 0;
            for (int j = i; j < i + jumlahKomponen; j++) {
                jumlah += deret[j];
            }

            if (jumlah < minimal) {
                minimal = jumlah;
            }

            if (jumlah > maksimal) {
                maksimal = jumlah;
            }
        }

        System.out.println("Nilai minimal: " + minimal);
        System.out.println("Nilai maksimal: " + maksimal);
    }
}
