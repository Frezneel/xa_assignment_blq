//4. Buatlah fungsi untuk menampilkan n bilangan prima pertama

//SELESAI

import java.util.Scanner;

public class Soal04_prima {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        int n = 0;
        while (flag_input) {
            System.out.println("Masukkan panjang(n) untuk membuat bilangan prima: ");
            n = sc.nextInt();
            if (n < 1){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }
        boolean flag = true;
        int i = 1;
        String dataPrima = "";
        int countData = 0;

        while (flag){
            int countPembagian = 0;
            for (int j = 1; j <= i; j++) {
                if (i%j == 0){
                    countPembagian++;
                }
            }

            if (countPembagian == 2 || countPembagian == 1){
                if (countData == 0){
                    dataPrima += i;
                }else {
                    dataPrima += ", " + i;
                }
                countData++;
            }

            if (countData >= n){
                flag = false;
            }else {
                i++;
            }
        }

        System.out.println("Data Prima (" + n +") : " + dataPrima);

    }
}
