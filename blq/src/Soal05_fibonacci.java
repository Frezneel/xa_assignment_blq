//5. Buatlah fungsi untuk menampilkan n bilangan Fibonacci pertama

//SELESAI

import java.util.Scanner;

public class Soal05_fibonacci {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean flag_input = true;
        int n = 0;
        while (flag_input) {
            System.out.println("Masukkan panjang(n) untuk membuat bilangan fibonacci: ");
            n = sc.nextInt();
            if (n < 1){
                System.out.println("Invalid input");
                System.out.println("Ulangi permintaan");
            }else {
                flag_input = false;
            }
        }
        boolean flag = true;
        String dataFibonacci = "";
        int[] data = new int[n];
        int countData = 0;
        int before = 0;

        while (flag) {
            if (countData == 0){
                data[0] = 1;
                countData++;
                dataFibonacci += data[0];
            }else {
                data[countData] = data[countData-1] + before;
                before = data[countData-1];
                dataFibonacci += ", " + data[countData];
                countData++;
            }
            if (countData >= n) {
                flag = false;
            }
        }

        System.out.println("Data Fibonacci (" + n +") : " + dataFibonacci);

    }
}
