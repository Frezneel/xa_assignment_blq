//15. Ubah format waktu "03:40:44 PM" menjadi format 24 jam (15:40:44)

//SELESAI

import java.util.Scanner;

public class Soal15_TimeConvert {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("====> Konversi Waktu");
        System.out.println("input format (12AM/PM) :");
        System.out.println("hh:mm:ss AM atau hh:mm:ss PM");

        String inputDate = sc.nextLine();

        String[] split = inputDate.split(" ");
        String[] nilai = split[0].split(":");

        int jam = Integer.parseInt(nilai[0]);
        String menit = nilai[1];
        String detik = nilai[2];
        String kondisi = split[1];

        if (kondisi.equals("PM")){
            System.out.println("Output jam : " + ((jam) + 12) + ":" + menit + ":" + detik);

        }else {
            System.out.println("Output jam : " + jam + ":" + menit + ":" + detik);
        }
    }
}
