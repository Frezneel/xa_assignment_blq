//17.	Hattori sedang berlatih untuk menjadi ninja yang baik dengan berlari melewati gunung dan lembah
//N N T N N N T T T T T N T T T N T N

//SELESAI


import java.util.Scanner;

public class Soal17_GunungLembah {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("==== HATORI NINJA ====");
        System.out.println("Contoh input = N N T N N N T T T T T N T T T N T N");
        String inputString = sc.nextLine();
        inputString = inputString.trim().toUpperCase();
        char[] inputChar = inputString.toCharArray();

        int hitungN = 0;
        int hitungT = 0;
        int gunung = 0;
        int lembah = 0;
        char sebelum;

        for (int i = 0; i < inputChar.length; i++) {
            char validChar = inputChar[i];
            if (validChar == 'N'){
                sebelum = 'N';
                hitungN ++;
                if (hitungN == hitungT){
                    lembah ++;
                    hitungN = 0;
                    hitungT = 0;
                }
            } else if (validChar == 'T') {
                sebelum = 'T';
                hitungT ++;
                if (hitungN == hitungT){
                    gunung ++;
                    hitungN = 0;
                    hitungT = 0;
                }
            }
        }
        System.out.println("Sisa Posisi = N-" + hitungN + " | T-" + hitungT);
        System.out.println("Output Hitung :");
        System.out.println("Gunung = " + gunung);
        System.out.println("Lembah = " + lembah);
    }
}
