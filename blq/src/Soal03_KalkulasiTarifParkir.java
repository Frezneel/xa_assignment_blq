//3. Buatlah fungsi untuk kalkulasi tarif parkir
//-	8 jam pertama: 1.000/jam
//-	8 jam 1 detik – 24 jam: 8.000 flat
//-	24 jam 1 detik – 8 jam kemudian: 15.000 flat + 1.000/jam

//SELESAI

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal03_KalkulasiTarifParkir {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        DateFormat df = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", new Locale("id"));
        System.out.println("+=====+ Sistem Parkir +====+");
        System.out.println("1. 8 jam pertama : 1000 per jam");
        System.out.println("2. 8 jam 1 detik – 24 jam : 8000 flat");
        System.out.println("3. 24 jam 1 detik – 8 jam kemudian : 15000 flat + 1000/jam");
        System.out.println("Contoh Input : 28 januari 2020 07:30:34");

        // Olah Input
        System.out.println("Input tanggal masuk :");
        String masukInput = sc.nextLine();
        boolean flag1 = true;
        Date masukDate = null;
        while (flag1){
            try{
                masukDate = df.parse(masukInput);
                flag1 = false;
            }catch (Exception e){
                System.out.println("Format input tidak sesuai");
                System.out.println("Contoh Input : 28 january 2020 07:30:34");
                System.out.println("Input tanggal masuk :");
                masukInput = sc.nextLine();
            }
        }
        System.out.println("Input tanggal keluar :");
        String keluarInput = sc.nextLine();
        Date keluarDate = null;
        boolean flag2 = true;
        while (flag2){
            try{
                keluarDate = df.parse(keluarInput);
                flag2 = false;
            }catch (Exception e){
                System.out.println("Format input tidak sesuai");
                System.out.println("Contoh Input : 28 january 2020 07:30:34");
                System.out.println("Input tanggal masuk :");
                keluarInput = sc.nextLine();
            }
        }

        long selisihWaktu = Math.abs(masukDate.getTime() - keluarDate.getTime());
        double selisihJam = (double) ((selisihWaktu / 1000) / 60) / 60;
        System.out.println("selisih = " + selisihJam + " jam");
        int output = 0;
        boolean flagHitung = true;

        while (flagHitung){
            if (selisihJam < 8){
                output += selisihJam * 1000;
                flagHitung = false;
            } else if (selisihJam > 8 && selisihJam < 24) {
                output += 8000;
                flagHitung = false;
            }else {
                output += 15000;
                selisihJam -= 24;
            }
        }
        System.out.println("Output = " + output);
    }
}
